import java.util.Scanner;

public class Auswahlstrukturbeispiele {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte die 1. Zahl ein: ");
		int zahl1 = myScanner.nextInt();
		
		System.out.println("Geben Sie bitte die 2. Zahl ein: ");
		int zahl2 = myScanner.nextInt();
		
		kleinezahl(zahl1, zahl2);
	}
	
	public static void kleinezahl(int zahl1, int zahl2) {
		if(zahl1 < zahl2) {
			System.out.println("Die Zahl " + zahl1 + " ist kleiner."); 
			
		} else if(zahl2 < zahl1) {
			System.out.println("Die Zahl " + zahl2 + " ist kleiner.");
		}
	}

}
